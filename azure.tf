resource "azurerm_resource_group" "rgroup" {
  name     = "rgroup"
  location = "West Europe"
}



resource "azurerm_virtual_network" "vnet" {
  name                = "vnet"
  resource_group_name = azurerm_resource_group.rgroup.name
  address_space = [ var.azure_vnet_cidr ]
  location = azurerm_resource_group.rgroup.location
}

resource "azurerm_subnet" "gw_subnet" {
  name                 = "GatewaySubnet"
  resource_group_name  = azurerm_resource_group.rgroup.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = [var.azure_gw_subnet_cidr]

}

resource "azurerm_subnet" "vm_subnet" {
  name                 = "VmSubnet"
  resource_group_name  = azurerm_resource_group.rgroup.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = [var.azure_vm_subnet_cidr]
  
}

#---------------------------------------------
# Public IP for Virtual Network Gateway
#---------------------------------------------
resource "azurerm_public_ip" "pip_gw" {
  name                = "azure-gw-gw-pip"
  location            = azurerm_resource_group.rgroup.location
  resource_group_name = azurerm_resource_group.rgroup.name
  allocation_method   = "Dynamic"
  
}

data "azurerm_public_ip" "gw_ipaddress" {
  name = "azure-gw-gw-pip"
  resource_group_name = azurerm_resource_group.rgroup.name
  

  depends_on = [
    azurerm_virtual_network_gateway.vpngw
  ]

  
}

#public IP for vm_public instance
resource "azurerm_public_ip" "vm_public" {
   name                         = "publicIPForInstance"
   location                     = azurerm_resource_group.rgroup.location
   resource_group_name          = azurerm_resource_group.rgroup.name
   allocation_method            = "Static"
 }





# Virtual network gateway
resource "azurerm_virtual_network_gateway" "vpngw" {
  name                = "vpngw"
  resource_group_name = azurerm_resource_group.rgroup.name
  location            = azurerm_resource_group.rgroup.location
  type                = "Vpn"
  vpn_type            = "RouteBased"
  sku                 = "Basic"
  active_active       = false
  enable_bgp          = false
  

 
  ip_configuration {
    name                          = "vnetGatewayConfig"
    public_ip_address_id          = azurerm_public_ip.pip_gw.id
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = azurerm_subnet.gw_subnet.id
  }

}

#---------------------------
# Local Network Gateway
#---------------------------
resource "azurerm_local_network_gateway" "yandex" {
  name                = "yandex"
  resource_group_name = azurerm_resource_group.rgroup.name
  location            = azurerm_resource_group.rgroup.location
  gateway_address     = yandex_vpc_address.yandex_addr.external_ipv4_address[0].address
  address_space       = [var.yandex_subnet_cidr]
}

#connection for IPSec
resource "azurerm_virtual_network_gateway_connection" "yandex" {
  name                = "yandex"
  location            = azurerm_resource_group.rgroup.location
  resource_group_name = azurerm_resource_group.rgroup.name

  type                       = "IPsec"
  virtual_network_gateway_id = azurerm_virtual_network_gateway.vpngw.id
  local_network_gateway_id   = azurerm_local_network_gateway.yandex.id

  shared_key = var.psk_password
}

# network interface for azure vm
resource "azurerm_network_interface" "nic" {
  name                = "vm-nic"
  location            = azurerm_resource_group.rgroup.location
  resource_group_name = azurerm_resource_group.rgroup.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.vm_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.vm_public.id
  }
}

resource "azurerm_linux_virtual_machine" "vm_public" {
  name                = "vm-public"
  resource_group_name = azurerm_resource_group.rgroup.name
  location            = azurerm_resource_group.rgroup.location
  size                = "Standard_F2"
  admin_username      = var.ssh_user
  network_interface_ids = [
    azurerm_network_interface.nic.id,
  ]

  admin_ssh_key {
    username   = var.ssh_user
    public_key = file(var.ssh_public_key)
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  depends_on = [
    azurerm_virtual_network_gateway.vpngw
  ]

}

