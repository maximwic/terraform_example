# создаем ресурс IP адрес, чтобы сделать конфигурацию до создания шлюза
resource "yandex_vpc_address" "yandex_addr" {
  name = "vpn-public-addr"

  external_ipv4_address {
    zone_id = "ru-central1-a"
  }
}

resource "yandex_vpc_network" "yandex" {
  name = "yandex-network"
}


# чтобы все работало прописываем маршрут в другую подсеть через шлюз
resource "yandex_vpc_route_table" "yandex-route" {
  network_id = yandex_vpc_network.yandex.id
  name = "yandex-route"

  static_route {
    destination_prefix = var.azure_vnet_cidr
    next_hop_address   = var.yandex_vpn_internal_ip
  }
}


resource "yandex_vpc_subnet" "yandex" {
  name           = "yandex-subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.yandex.id
  v4_cidr_blocks = [var.yandex_subnet_cidr]
  route_table_id = yandex_vpc_route_table.yandex-route.id
  
}




#создаем шлюз
resource "yandex_compute_instance" "vpn_gw" {
  name = "vpn-gw"
  resources {
    cores  = 2
    memory = 2
  }
#в качестве образа выбираю ipsec от Яндекса
  boot_disk {
    initialize_params {
      image_id = "fd8touptelufms7frnlm"
    }
  }

# при создании сети, берем ранее созданный внешний IP и привязываем указанный в параметрах внутренний IP
  network_interface {
    subnet_id      = yandex_vpc_subnet.yandex.id
    ip_address     = var.yandex_vpn_internal_ip
    nat            = true
    nat_ip_address = yandex_vpc_address.yandex_addr.external_ipv4_address[0].address
  }


  metadata = {
    ssh-keys = "${var.ssh_user}:${file(var.ssh_public_key)}"

  }

  lifecycle {
    replace_triggered_by = [
      # Replace `aws_appautoscaling_target` each time this instance of
      # the `aws_ecs_service` is replaced.
      local_file.ipsec_conf,
      local_file.ipsec_secrets
    ]
  }

# копируем в шлюз файл с настройками
  provisioner "file" {
    connection {
      type        = "ssh"
      host        = yandex_vpc_address.yandex_addr.external_ipv4_address.0.address
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
    }
    source      = "${path.module}/templates/yandex_ipsec.conf.tmpl"
    destination = "ipsec.conf"
  }

# копируем в шлюз файл с паролями
  provisioner "file" {
    connection {
      type        = "ssh"
      host        = yandex_vpc_address.yandex_addr.external_ipv4_address.0.address
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
    }
    source      = "${path.module}/templates/yandex_ipsec.secrets.tmpl"
    destination = "ipsec.secrets"
  }

# доступ по ССХ у нас не рутовый, поэтому делаем отдельное подключение для копирования файлов через судо и перезапускаем демон
  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = yandex_vpc_address.yandex_addr.external_ipv4_address.0.address
      user        = var.ssh_user
      private_key = file(var.ssh_private_key)
    }

    inline = [
      "sudo cp ipsec.conf /etc/",
      "sudo cp ipsec.secrets /etc/",
      "sudo systemctl restart strongswan-starter",
    ]
  }


}

# создаем целевую машину
resource "yandex_compute_instance" "vm_yandex" {
  name = "yandex-vm"
  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ofg98ci78v262j491"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.yandex.id
  }

  metadata = {
    ssh-keys = "${var.ssh_user}:${file(var.ssh_public_key)}"

  }
  
}



# для того, чтобы началась создаваться машинка-шлюз, надо чтобы были готовы файлы конфигурации. Создаю их через отдельный ресурс и кладу в папку Темплейтс
resource "local_file" "ipsec_conf" {
  filename        = "${path.module}/templates/yandex_ipsec.conf.tmpl"
  file_permission = "666"
  # VPN шлюз в ажуре создается 45 минут, выделение ip адреса происходит после создания шлюза + немного времени. 
  
  
  content         = <<-EOT
    config setup
        charondebug="all"
        uniqueids=yes
        strictcrlpolicy=no
    conn cloud-to-hq
      authby=secret
      left=%defaultroute
      leftid=${yandex_vpc_address.yandex_addr.external_ipv4_address[0].address}
      leftsubnet=${var.yandex_subnet_cidr}
      right=${data.azurerm_public_ip.gw_ipaddress.ip_address}
      rightsubnet=${var.azure_vnet_cidr}
      ike=aes256-sha2_256-modp1024!
      esp=aes256-sha2_256!
      keyingtries=0
      ikelifetime=1h
      lifetime=8h
      dpddelay=30
      dpdtimeout=120
      dpdaction=restart
      auto=start
EOT
}

resource "local_file" "ipsec_secrets" {
  filename        = "${path.module}/templates/yandex_ipsec.secrets.tmpl"
  file_permission = "666"
  content         = <<-EOT
    ${yandex_vpc_address.yandex_addr.external_ipv4_address[0].address} ${data.azurerm_public_ip.gw_ipaddress.ip_address} : PSK "${var.psk_password}"
EOT
}








