
output "azure_vm_public_ip_address" {
  value = data.azurerm_public_ip.gw_ipaddress.ip_address
}


output "azure_gw_public_ip_address" {
  value = azurerm_public_ip.pip_gw.ip_address
}

output "yandex_gw_public_ip_address" {
  value = yandex_vpc_address.yandex_addr.external_ipv4_address[0].address
}

output "yandex_vm_internal_ip_address" {
  value = yandex_compute_instance.vm_yandex.network_interface.0.ip_address 
}
