variable "azure_gw_subnet_cidr" {
  
}

variable "azure_vm_subnet_cidr" {
  
}

variable "azure_vnet_cidr" {
  
}

variable "yandex_subnet_cidr" {
  
}

variable "yandex_vpn_internal_ip" {
  
}

variable "psk_password" {
  
}

variable "ssh_public_key" {
  
}

variable "ssh_private_key" {
  
}

variable "ssh_user" {
  
}